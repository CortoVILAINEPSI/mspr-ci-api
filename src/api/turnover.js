// Début Configuration firestore
const admin = require('firebase-admin');
const serviceAccount = require('../../keyfile.json');

try {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://mspr-ci-database.firebaseio.com'
  });
} catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack);
  }
}
const db = admin.firestore();
// Fin Configuration firestore

const customersCollection = db.collection('customers');

require('dotenv').config();

module.exports = {
  getTurnovers() {
    return new Promise((resolve, reject) => {
      customersCollection
        .get()
        .then((querySnapshot) => {
          if (!(querySnapshot.size === 0)) {
            let turnoverht = 0;
            let index = 0;

            querySnapshot
              .forEach(async (doc) => {
                if (doc.exists) {
                  const queryPurchases = customersCollection.doc(doc.id).collection('purchases');

                  await queryPurchases
                    .get()
                    .then((querySnapshotPurchase) => {
                      if (!(querySnapshotPurchase.size === 0)) {
                        querySnapshotPurchase
                          .forEach((purchase) => {
                            const { quantity } = purchase.data();
                            const { unitPrice } = purchase.data();

                            turnoverht += quantity * unitPrice;
                            turnoverht = parseFloat(turnoverht.toFixed(2))
                          });
                      }
                    });

                  index += 1;

                  if (querySnapshot.size === index) {
                    const turnoverttc = parseFloat((turnoverht * 1.2).toFixed(2));
                    
                    const result = {
                      turnoverht,
                      turnoverttc
                    };

                    resolve(result);
                  }
                }
              });
          } else {
            resolve('Aucun document');
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
};
