const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const multer = require('multer');
const upload = multer({dest: '/uploads/'});
const csv = require('csv-parser');
const fs = require('fs');

require('dotenv').config();

const app = express();
const bodyParser = require('body-parser');

// Swagger
const swaggerUi = require('swagger-ui-express');
// Fin Swagger

// MSPR-Maintenance

var admin2 = require("firebase-admin");

var serviceAccount2 = require("../keyfile2.json");

try {
  admin2.initializeApp({
    credential: admin2.credential.cert(serviceAccount2),
    databaseURL: 'https://mspr-ci-database.firebaseio.com'
  });
} catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack);
  }
}

var db = admin2.firestore();

// Fin MSPR-Maitenance

const swaggerDocument = require('./swagger.json');

app.use('/mspr-ci/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const middlewares = require('./middlewares');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan('dev'));
app.use(helmet());
app.use(cors());

const customerFile = require('./api/customer');
const purchaseFile = require('./api/purchase');
const turnoverFile = require('./api/turnover');

app.get('/mspr-ci/customers/:idCustomer', (req, res) => {
  customerFile
    .getCustomerById(req.params.idCustomer)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

app.get('/mspr-ci/customers', (req, res) => {
  customerFile
    .getCustomers()
    .then((result) => {
      res.json(result);
    }).catch((err) => {
      res.send(err);
    });
});

app.post('/mspr-ci/customers', (req, res) => {
  customerFile
    .addCustomer(req.body.firstname, req.body.lastname)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

app.get('/mspr-ci/purchases/:idCustomer', (req, res) => {
  purchaseFile
    .getPurchasesByCustomerId(req.params.idCustomer)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

app.post('/mspr-ci/purchases', (req, res) => {
  purchaseFile
    .addPurchase(req.body.customer_id, req.body.product, req.body.quantity, req.body.unitPrice)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

app.get('/mspr-ci/turnovers', (req, res) => {
  turnoverFile
    .getTurnovers()
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

/* MSPR-MAINTENANCE */

app.post('/mspr-ci/csv', upload.single('csv'), (req, res) => {
  
  let file = req.file;
  let fileName = file.originalname;
  const results = [];
 
  fs.createReadStream(file.path)
    .pipe(csv({separator: ';'}))
    .on('data', (data) => results.push(data))
    .on('end', () => { 
      let newCsv = {
        fileName,
        results
      }
      db.collection('csv').add(newCsv);
    });

  res.json({
    message: "Le fichier CSV a bien été ajouté"
  })
});

/* FIN MSPR-MAINTENANCE*/

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
