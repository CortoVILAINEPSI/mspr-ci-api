// Début Configuration firestore
const admin = require('firebase-admin');
const serviceAccount = require('../../keyfile.json');

try {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://mspr-ci-database.firebaseio.com'
  });
} catch (err) {
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack);
  }
}
const db = admin.firestore();
// Fin Configuration firestore

const customersCollection = db.collection('customers');

require('dotenv').config();

module.exports = {
  getCustomerById(customer_id) {
    return new Promise((resolve, reject) => {
      const query = customersCollection.doc(customer_id);

      query
        .get()
        .then((doc) => {
          if (doc.exists) {
            const result = [];

            result.push({
              customer_id: doc.id,
              firstname: doc.data().firstname,
              lastname: doc.data().lastname
            });

            resolve(result);
          } else {
            resolve('Aucun document');
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  getCustomers() {
    return new Promise((resolve, reject) => {
      customersCollection
        .get()
        .then((querySnapshot) => {
          if (!(querySnapshot.size === 0)) {
            const result = [];

            querySnapshot
              .forEach((doc) => {
                result.push({
                  customer_id: doc.id,
                  firstname: doc.data().firstname,
                  lastname: doc.data().lastname
                });
              });

            resolve(result);
          } else {
            resolve('Aucun client');
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  addCustomer(firstname, lastname) {
    return new Promise((resolve, reject) => {
      const customer = {
        firstname,
        lastname
      };

      customersCollection.add(customer)
        .then(
          resolve({ message: `Ajout de l'utilisateur ${firstname} ${lastname}` })
        )
        .catch((err) => {
          reject(err);
        });
    });
  }
};
