const request = require('supertest')
const app = require('../src/app')
//const webhook = require("webhook-discord")
//const Hook = new webhook.Webhook("https://discordapp.com/api/webhooks/677156795604533276/rRvL3a-FCiuE6RyWA-tY08eTx2WwVLMOa8PlDR_xKoyaR1xCQwB8HoefYaSYuECk5uRA")

// describe('GET /', () => {
//   it('Verification GET /mspr-ci/test', function(done) {
//     request(app)
//       .get('/mspr-ci/test')
//       .set('Accept', 'application/json')
//       .expect(404)
//       .end(() => {
//         Hook.success("GET /mspr-ci/test", "Succès du test unitaire GET /mspr-ci/test pour tester l'erreur 404")
//         done()
//       })
//   })
// })

// describe('GET /', () => {
//   it('Verification GET /mspr-ci/customers/{userId}', function(done) {
//     request(app)
//       .get('/mspr-ci/customers/zYpb4pQ6AlTJ0i9bDk6r')
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200)
//       .end(() => {
//         Hook.success("GET /mspr-ci/customers/{userId}", "Succès du test unitaire GET /mspr-ci/customers/{userId}")
//         done()
//       })
//   })
// })

// describe('GET /', () => {
//   it('Verification 0 document GET /mspr-ci/customers/{userId}', function(done) {
//     request(app)
//       .get('/mspr-ci/customers/testId')
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200)
//       .end(() => {
//         Hook.success("GET /mspr-ci/customers/{userId}", "Succès du test unitaire GET /mspr-ci/customers/{userId} avec un mauvais id")
//         done()
//       })
//   })
// })

// describe('GET /', () => {
//   it('Verification GET /mspr-ci/customers', function(done) {
//     request(app)
//       .get('/mspr-ci/customers')
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200)
//       .end(() => {
//         Hook.success("GET /mspr-ci/customers", "Succès du test unitaire GET /mspr-ci/customers")
//         done()
//       })
//   })
// })

// describe('POST /', () => {
//   it('Verification POST /mspr-ci/customers', function(done) {
//     request(app)
//       .post('/mspr-ci/customers')
//       .send({
//         firstname: "firstname",
//         lastname: "lastname"
//       })
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200, {
//         message: "Ajout de l'utilisateur firstname lastname"
//       })
//       .end(() => {
//         Hook.success("POST /mspr-ci/customers", "Succès du test unitaire POST /mspr-ci/customers")
//         done()
//       })
//   })
// })

// describe('GET /', () => {
//   it('Verification GET /mspr-ci/purchases/{userId}', function(done) {
//     request(app)
//       .get('/mspr-ci/purchases/zYpb4pQ6AlTJ0i9bDk6r')
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200)
//       .end(() => {
//         Hook.success("GET /mspr-ci/purchases/{userId}", "Succès du test unitaire GET /mspr-ci/purchases/{userId}")
//         done()
//       })
//   })
// })

// describe('GET /', () => {
//   it('Verification GET /mspr-ci/purchases/{userId}', function(done) {
//     request(app)
//       .get('/mspr-ci/purchases/test')
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200)
//       .end(() => {
//         Hook.success("GET /mspr-ci/purchases/{userId}", "Succès du test unitaire GET /mspr-ci/purchases/{userId} avec un mauvais id")
//         done()
//       })
//   })
// })

// describe('POST /', () => {
//   it('Verification POST /mspr-ci/purchases', function(done) {
//     request(app)
//       .post('/mspr-ci/purchases')
//       .send({
//         customer_id: "zYpb4pQ6AlTJ0i9bDk6r",
//         product: "patate",
//         quantity: 2,
//         unitPrice: 1.5
//       })
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200, {
//         message: "Ajout de l'achat de 2 patate(s) à 1.5€ chacun"
//       })
//       .end(() => {
//         Hook.success("POST /mspr-ci/purchases", "Succès du test unitaire POST /mspr-ci/purchases")
//         done()
//       })
//   })
// })

// describe('POST /', () => {
//   it('Verification POST /mspr-ci/purchases', function(done) {
//     request(app)
//       .post('/mspr-ci/purchases')
//       .send({
//         customer_id: "testId",
//         product: "patate",
//         quantity: 2,
//         unitPrice: 1.5
//       })
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200)
//       .end(() => {
//         Hook.success("POST /mspr-ci/purchases", "Succès du test unitaire POST /mspr-ci/purchases avec un mauvais customer_id")
//         done()
//       })
//   })
// })

// describe('GET /', () => {
//   it('Verification GET /mspr-ci/turnovers', function(done) {
//     request(app)
//       .get('/mspr-ci/turnovers')
//       .set('Accept', 'application/json')
//       .expect('Content-Type', /json/)
//       .expect(200)
//       .end(() => {
//         Hook.success("GET /mspr-ci/turnovers", "Succès du test unitaire GET /mspr-ci/turnovers")
//         done()
//       })
//   })
// })

describe('POST /', () => {
  it('Verification POST /mspr-ci/csv', function(done) {
    request(app)
      .post('/mspr-ci/csv')
      .send({
        csvName: "value1",
        csvValues: [
          {
          IdentifiantUnique: "aaa",
          NomInformation: "aaa",
          ValeurInformation: "aaa"
        },
        {
          IdentifiantUnique: "bbb",
          NomInformation: "bbb",
          ValeurInformation: "bbb"
          }
        ]
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end(() => {
        done()
      })
  })
})